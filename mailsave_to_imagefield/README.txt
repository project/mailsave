Mailsave to Imagefield
-----------------------
Mailsave to Imagefield is an extension for mailsave that works with the CCK
imagefield.module.

Once activated, if the user has "convert to imagefield" privileges then any
images (with acceptable extensions) found in the mail will be saved as CCK
imagefield items inside the first imagefield found in the node.

Thanks to Moonshine (drupal.org/user/133705) for contributing this module!

